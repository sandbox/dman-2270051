<?php
/**
 * @file
 * feeds_tamper_file_fetcher_demo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feeds_tamper_file_fetcher_demo_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}
