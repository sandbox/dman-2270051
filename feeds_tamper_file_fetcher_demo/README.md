This is a simple Demo to show how images can be downloaded from a common RSS source.

## Requirements:

* Feeds XML Parser - to scrape the RSS element(s)
* Features - to preload this Feeds Importer definition.
* A Standard Drupal install with 'Article' content type, 'image' field and 'tags' category.

## Usage 
After installing this module, a new Feeds Importer should be shown 
 at your `/import/` URL.

Running 'import' on that will pull down items from a Flickr tag search
 Page: https://www.flickr.com/photos/tags/drupalcon/
 Feed: https://api.flickr.com/services/feeds/photos_public.gne?tags=drupalcon
 
This is expected to create 'Article' nodes that have images attached 
 using the standard 'image' field attachment.

## How it's done

Inspect the settings you'll see for the parser at 
`/admin/structure/feeds/feeds_tamper_file_fetcher_demo/settings/FeedsXPathParserXML`
and the processor mappings at 
`/admin/structure/feeds/feeds_tamper_file_fetcher_demo/mapping`

