To Import media entities (images) directly from a Media-rss stream.
===================================================================

### From feeds that use media:content or rss 2.0 enclosure.

Given that:

We have access to a nice RSS fed that enumerates the content of an
image gallery.

We need to:

* Get a list of images from a remote source (Feeds Fetcher)
* Extract info like URL and Caption from that data (Feeds XPath Parser)
* Fetch the remote file itself ( **Feeds Tamper File Fetcher** )
* Create a local entity for each file (Feeds Entity Processor (-dev) + File Entity )
* Attach each image to a gallery based on source metadata.
  ( **Feeds Tamper File Fetcher** + Media Gallery)


Requirements
------------

In order to make the most use of existing re-usable parts, and be extendable
in all other similar use-cases, we use a lot of alternative Feeds-related
components.

*  **media** module 2.x not 1.x.
    which uses **file_entity** module.
*  **media_gallery**
    which uses **multiform** , and also requires **plupload**.
*  **feeds** module.
    Which requires **job_scheduler**
    dev or stable post Jan 2014 https://drupal.org/node/1033202#comment-8372035 .
    7.x-2.0-alpha8 is NOT good enough
*  NOT feeds_mediarss NOR mediarss  parser.
  Just use **feeds_xpath_parser** to detect enclosures and media metadata

*  **feeds_tamper_file_fetcher** to actually transfer and import the files listed.
    Which requires **feeds_tamper**.
  feeds_tamper_file_fetcher may be git-only until stable.

        drush dl media-2.x file_entity media_gallery-2.x  multiform plupload feeds-2.x job_scheduler feeds_xpathparser feeds_tamper
        drush en feeds_ui media_gallery entity feeds_tamper_admin_ui feeds_tamper_file_fetcher feeds_xpathparser
  
*  Other configs may be required by this big set of stuff if you are not
   already using them. eg plupload need a library.

Setup
-----
  
* Create a feed, eg "image_gallery_from_rss".
* Select "XPath XML parser".
* Select "File entity processor - EXPERIMENTAL".
* Configure the appropriate bundle for the file entity (Image).
* Edit mappings:
    * in /admin/structure/feeds/image_gallery_from_rss/mapping.
    * Create 3 xpath sources (xpath:0, xpath:1, xpath:2),
      destined to *URI*, *GUID*, *filename* and *description* respectively.
    * Filename (not 'file name', 'title' or 'title text' -
      these don't actually help on a base file_entity.)
* Edit the xpath parser context settings and patterns
  at /admin/structure/feeds/image_gallery_from_rss/settings/FeedsXPathParserXML .
    *  Context: /rss/channel/item
    *  xpath:0 uri = string(media:content/@url)
    *  xpath:1 guid = link
    *  xpath:2 filename = title
    *  xpath:3 description = description

* Additional/optional : add image to media_gallery.
  Requires media_module installed.
    * Given that the source data has a 'category' which identifies a gallery
      by name..
    * Add another xpath _mapping_ that targets **Media Gallery : name**
      (this new target is supplied by feeds_tamper_file_fetcher)
    * Configure that xpath _pattern_ to 'category'
    * Now, when importing, a media gallery will be found or created and that
      image added to it. Multiples are supported for each category.
  
* Now 'tamper' with the URI value and add 'file fetcher' as an action
  at /admin/structure/feeds/image_gallery_from_rss/tamper

  Additional settings can be used to add or remove additional path fragments
  when copying locally to create appropriate filepaths.

* If the image we are fetching is a derivative of some sort and we would rather
  have the original -
  look into doing some additional string remplacement at this point.
  eg feeds_tamper
  
* Test it against a valid Atom/MediaRSS feed by visiting /import and entering
  an URL like

        http://thevintageaviator.co.nz/piclens/imagegallery/61


Troubleshooting
---------------

All the tools are in the Feeds family, so look there for hints.
If the source data is not identical with the data, you can probably still
adapt the xpath rules to get the same effect.
There is a good amount of info in the feeds log (about item data processing)
and in the watchdog (the remote file fetching).
You may find that it's server firewall or proxy issues if the http fetching
is failing.
